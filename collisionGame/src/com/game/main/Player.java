package com.game.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Player extends GameObject {

	Handler handler;

	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.mass = 1;
		this.handler = handler;
	}
	
	public void tick() {	
		setRealVelX();
		setRealVelY();		
		collisionPvP(ID.PlayerOne, ID.PlayerTwo, 60, 1);
		x = stayBound(x, Game.WIDTH - 25, 0);
		y = stayBound(y, Game.HEIGHT - 50, 0);
		move();
	}
 
	public void move() {
		if (!startTickCount) {
			x += getVelX();
			y += getVelY();
		}
	}
	
	public void collisionPvP(ID One, ID Two, int durationCount, int decreaseVel) {	
		if (startTickCount)
			this.tickCount++;
		if (colX != 0 || colY != 0)
			System.out.print(id + " |colX: " + colX + " |colY: " + colY);
		if (id == One) {	
			for (int i = 0; i < handler.object.size(); i++) {	
				GameObject tempObject = handler.object.get(i);
				if (tempObject.getId() == Two && !startTickCount) {
					if (getBounds().intersects(tempObject.getBounds())) {
						//collision code
						this.startTickCount = true;
						tempObject.startTickCount = true;
						this.durationCount = durationCount;
						tempObject.durationCount = durationCount;					
						int collisionX = ( (mass * getVelX() + (tempObject.mass * (2 * tempObject.getVelX() - getVelX()))) / (mass + tempObject.mass) );
						int collisionY = ( (mass * getVelY() + (tempObject.mass * (2 * tempObject.getVelY() - getVelY())))/(mass + tempObject.mass) );
						int collisionX2 = ( (tempObject.mass * tempObject.getVelX() + (mass * (2 * getVelX() - tempObject.getVelX())))/(mass + tempObject.mass) );
						int collisionY2 = ( (tempObject.mass * tempObject.getVelY() + (mass * (2 * getVelY() - tempObject.getVelY())))/(mass + tempObject.mass) );
						tempObject.setColX(collisionX2);
						tempObject.setColY(collisionY2);
						setColX(collisionX);
						setColY(collisionY);			
					}
				}
			}
		}
		
		if (startTickCount) {
			x += colX;
			y += colY;
			if ((colX != 0) && (tickCount % 10 == 0)) {
				int aX = (Math.abs(colX));
				colX = (aX - decreaseVel) * (aX/colX);
			}
			if ((colY != 0) && (tickCount % 10 == 0)) {
				int aY = (Math.abs(colY));
				colY = ((aY - decreaseVel) * (aY/colY));
			}
			if (durationCount == tickCount) {
				startTickCount = false;
				this.tickCount = 0;
			}
		}
	}	
		
	public void collisionEnemy() {
		for (int i = 0; i < handler.object.size(); i++) {			
			GameObject tempObject = handler.object.get(i);		
			if(tempObject.getId() == ID.Enemy) {
				if(getBounds().intersects(tempObject.getBounds())) {
					//collision code
				}
			}
		}
	}
	
	public void render(Graphics g) {
		if(id == ID.PlayerOne)
			g.setColor(Color.blue);
		else {
			g.setColor(Color.yellow);   
		}
		g.fillRect(x, y, 10, 10); 
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, 10, 10);
	}
}
