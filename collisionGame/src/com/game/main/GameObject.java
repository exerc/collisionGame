package com.game.main;

import java.awt.Graphics;
import java.awt.Rectangle;

public abstract class GameObject {

	protected int x, y;
	protected ID id;
	protected int velXP, velXN, velYP, velYN, velY, velX, colX, colY, realVelY, realVelX, mass;
	public boolean startTickCount = false;
	public int tickCount = 0;
	public int durationCount = 0;
	
	public GameObject(int x, int y, ID id) {
		this.x = x;
		this.y = y;
		this.id = id;
	}
	
	public abstract void tick();
	public abstract void render(Graphics g);
	public abstract Rectangle getBounds();
	
	public static int stayBound(int var, int max, int min) {
		if (var >= max) 
			return var = max;
		if (var <= min)
			return var = min;
		else 
			return var;
	}
	
	public void setRealVelX() {
		if (startTickCount)
			this.realVelX = colX; 
		else this.realVelX = velX; 
	}
		
	public void setRealVelY() {
		if (startTickCount)
			this.realVelY = colY; 
		else this.realVelY = velY; 
	}
	
	public int getRealVelX() {
		return this.velX;
	}
	
	public int getRealVelY() {
		return this.velY;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setID(ID id) {
		this.id = id;
	}
	
	public ID getId() {
		return id;
	}
	
	public void setVelXP (int VelXP) {
		this.velXP = VelXP;
	}
	
	public void setVelXN(int velXN) {
		this.velXN = velXN;
	}
	
	public int getVelX() {
		return (velX = velXP + velXN);
	}
	
	public void setVelYP(int velYP) {
	 this.velYP = velYP;
	}

	public void setVelYN (int velYN) {
		this.velYN = velYN;
	}
	
	public int getVelY() {
		return (velY = velYP + velYN);
	}
	
	public void setColX(int colX) {
		this.colX = colX;
	}

	public void setColY(int colY) {
		this.colY = colY;
	}
	
	public void resetVelXY() {
		this.velYP = 0;
		this.velYN = 0;
		this.velXN = 0;
		this.velXP = 0;
	}
}
