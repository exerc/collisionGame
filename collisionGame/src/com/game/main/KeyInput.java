package com.game.main;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter{
	
	private Handler handler;
	
	public KeyInput(Handler handler) {
		this.handler = handler;		
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		new Thread(new Runnable() {
			@Override
			public void run() {				
				for (int i = 0; i < handler.object.size(); i++) {
					GameObject tempObject = handler.object.get(i);		
					if (tempObject.getId() == ID.PlayerOne) {	
						if (key == KeyEvent.VK_W) {
							tempObject.setVelYN(-5);
							System.out.println("W:" + tempObject.getVelX() + " " + tempObject.getVelY());
						}
						if (key == KeyEvent.VK_S) {
							tempObject.setVelYP(5);
							System.out.println("S:" + tempObject.getVelX() + " " + tempObject.getVelY());
						}
						if (key == KeyEvent.VK_D) {
							tempObject.setVelXP(5);
							System.out.println("D:" + tempObject.getVelX() + " " + tempObject.getVelY());
						}
						if (key == KeyEvent.VK_A) {
							tempObject.setVelXN(-5);
							System.out.println("A:" + tempObject.getVelX() + " " + tempObject.getVelY());
						}
					}	
					
					if (tempObject.getId() == ID.PlayerTwo) {	
						if (key == KeyEvent.VK_UP) {
							tempObject.setVelYN(-5);
							System.out.println(tempObject.getVelX() + " " + tempObject.getVelY());
						}
						if (key == KeyEvent.VK_DOWN) {
							tempObject.setVelYP(5);
							System.out.println(tempObject.getVelX() + " " + tempObject.getVelY());
						}
						if (key == KeyEvent.VK_RIGHT) {
							tempObject.setVelXP(5);
							System.out.println(tempObject.getVelX() + " " + tempObject.getVelY());
						}
						if (key == KeyEvent.VK_LEFT) {
							tempObject.setVelXN(-5);
							System.out.println(tempObject.getVelX() + " " + tempObject.getVelY());
						}
					}
				}
			}
		}).start();
	}
	
	public void keyReleased(KeyEvent e) {		
		new Thread(new Runnable() {
			@Override
			public void run() {
				int key = e.getKeyCode();	
				for (int i = 0; i < handler.object.size(); i++) {
					GameObject tempObject = handler.object.get(i);
					if (tempObject.getId() == ID.PlayerOne) {
						if (key == KeyEvent.VK_W) {
							tempObject.setVelYN(0);
						}
						if (key == KeyEvent.VK_S) {
							tempObject.setVelYP(0);
						}
						if (key == KeyEvent.VK_D) {
							tempObject.setVelXP(0);
						}
						if (key == KeyEvent.VK_A) {
							tempObject.setVelXN(0);
						}
					}
			
					if (tempObject.getId() == ID.PlayerTwo) {			
						if (key == KeyEvent.VK_UP)
							tempObject.setVelYN(0);
						if (key == KeyEvent.VK_DOWN)
							tempObject.setVelYP(0);
						if (key == KeyEvent.VK_RIGHT)
							tempObject.setVelXP(0);
						if (key == KeyEvent.VK_LEFT)
							tempObject.setVelXN(0);
					}		
				}	
			}
		}).start();		
	}

}
